#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
 
int c, i, errorcode, linenr, recordingday, mmsb, msb, lsb, n, line, hours,
  minutes, seconds, rectime, valid, status, d, bytecount1, bytecount2, count;
int padding = 165, error = 0, emptyfile = 0, option_index = 0,
  crectime = 0, srectime = 0, Srectime = 0, Erectime = 8640000,
  reclen = 0, erectime = 8640000, lineno = -2, acclineno = 0, 
  validno = -1, qlines = 0, v = 0, mode = 0, qxlines = 0, a5 = 165;
long llength;
FILE *f, *g;
char *inputfile = NULL, *outputfile = NULL;
unsigned char *y, *z, *zx;

void printusage(int mode) {
  if (mode == 0) {
    /* We print to standard output*/
    printf ("Usage: ffprune [ -i input file -o output file ]\n"
    "Prunes Final Format files\n"
    "             [ -h this help listing ]\n"
    "             [ -v prints the version ]\n"
    "             [ -l line numbers ]\n"
    "             [ -x exclude line numbers ]\n"
    "             [ -s start time in tens of millisseconds ]\n"
    "             [ -e end time in tens of milliseconds ]\n"
    "             [ -S start time in HH:MM:SS[.ss] format ]\n"
    "             [ -E end time in HH:MM:SS[.ss] format ]\n"
    "             [ -a start count of input file ]\n"
    "             [ -z stop count of input file ]\n"
    "             [ -b start count of output file ]\n"
    "             [ -y stop count of output file ]\n");
  } else {
    /* We print to standard error*/
    fprintf (stderr,"Usage: ffprune [ -i input file -o output file ]\n"
    "Prunes Final Format files\n"
    "             [ -h this help listing ]\n"
    "             [ -v prints the version ]\n"
    "             [ -l line numbers ]\n"
    "             [ -x exclude line numbers]\n"
    "             [ -s start time in tens of millisseconds ]\n"
    "             [ -e end time in tens of milliseconds ]\n"
    "             [ -S start time in HH:MM:SS[.ss] format ]\n"
    "             [ -E end time in HH:MM:SS[.ss] format ]\n"
    "             [ -a start count of input file ]\n"
    "             [ -z stop count of input file ]\n"
    "             [ -b start count of output file ]\n"
    "             [ -y stop count of output file ]\n");
  }
}

int getreclen() {
  bytecount1 = fgetc(f);
  if (bytecount1 != -1 ) {
    bytecount2 = fgetc(f);
    reclen= bytecount2 + bytecount1 * 256;
    /* emptyfile will remain 0 only if the file is totally empty */
    if ( emptyfile == 0 ) {
      emptyfile = 1;
    }
  } 
  else {
    reclen = -1;
  }
  return reclen;
}

int geterrorcode() {
  c = fgetc(f);
  return c;
}

int getlinenr() {
  c = fgetc(f);
  return c;
}

/* recordingday is not used*/
int getrecordingday() {
  c = fgetc(f);
  return c;
}

int getmmsb() {
  c = fgetc(f);
  return c;
}

int getmsb() {
  c = fgetc (f);
  return c;
}

int getlsb() {
  c = fgetc (f);
  return c;
}

void hexdump(int reclen) {
  for (i=0; i<reclen-12; i++)
  {
    c = fgetc(f);
    if (c != -1 ) {
      y[i] = c;
    }
    else {
      reclen = -1;
    }
  }
}

int printdump(unsigned char *y) {
  /* If we have not opened the output file yet, open it now */
  fprintf (g, "%c", bytecount1);
  fprintf (g, "%c", bytecount2);
  fprintf (g, "%c", errorcode);
  fprintf (g, "%c", linenr);
  fprintf (g, "%c", recordingday);
  fprintf (g, "%c", mmsb);
  fprintf (g, "%c", msb);
  fprintf (g, "%c", lsb);
  for (i=0; i<reclen-12; i++)
  {
    fprintf (g, "%c", y[i]);
  }
  fprintf (g, "%c", a5);
  fprintf (g, "%c", a5);
  fprintf (g, "%c", a5);
  fprintf (g, "%c", a5);
  return 0;
}

int getpadding() {
  for (i = 0; i < 4 ; i++) {
    c = fgetc(f);
    if (c != 165) {
      padding = 0;
    }
  }
  return padding;
}

int getcline(int n) {
  if ( n >= 48 && n <= 57 ) {
    /* 0 - 9 */
    line = n - 48;
  }
  else if ( n >= 65 && n <= 70 ) {
    /* A - F*/
    line = n - 55;
  }
  else if ( n >= 97 && n <= 102 ) {
    /* a - f */
    line = n - 87;
  }
  else {
    return -1;
  }
  return line;
}

int checkargument(char* optarg) {
  llength = strlen(optarg);
  if (llength  >= 8 ) {
    for (i = 0; i < llength; i++) {
      if (i != 2 && i != 5 && i != 8) {
        /* We expect 0-9 here */
        if (optarg[i] >= 48 && optarg[i] <= 57){
        }
        else {
          fprintf (stderr,"Time must be in HH:MM:SS[.ss] format\n");
          return 1;
        }
      }
      else if ( i == 8  ){
        if (optarg[8] != 46 ) {
          /* We expect a . here */
          fprintf (stderr,"Time must be in HH:MM:SS[.ss] format\n");
          return 1;
        }
      }
      else {
        /* We expect a : here */
        if (optarg[i] != 58 ) {
          fprintf (stderr,"Time must be in HH:MM:SS[.ss] format\n");
          return 1;
        }
      }
    }
  }
  else {
    fprintf (stderr,"Time must be in HH:MM:SS[.ss] format\n");
    return 1;
  }  
  return 0;
}

int getrectime (char* optarg) {
  llength = strlen(optarg);
  for (i=0; i < llength; i++) {
    optarg[i] = optarg[i] - 48;
  }
  /* hours must be less than 24
     minutes and seconds must be less than 60 */
  hours = optarg[0] * 10 + optarg[1];
  minutes = optarg[3] * 10 + optarg[4];
  seconds = optarg[6] * 10 + optarg[7];
  if (hours > 23 ) {
    fprintf (stderr,"Hours must be less than 24\n");
    return -1;
  }
  if (minutes > 59) {
    fprintf (stderr,"Minutes must be less than 60\n");
    return -1;
  }
  if (seconds > 59) {
    fprintf (stderr,"Seconds must be less than 60\n");
    return -1;
  }
  /* crectime is in tens of milliseconds */
  crectime = hours * 360000 + minutes * 6000 + seconds * 100;
  if (llength >= 10 ) {
   crectime = crectime + optarg[9] * 10;
  }
  if (llength >= 11 ) {
   crectime = crectime + optarg[10];
  }
  return crectime;
}


int checkdigits(char* optarg) {
  llength = strlen(optarg);
  for (i = 0; i < llength; i++) {
    /* We expect 0-9 here */
    if (optarg[i] < 48 || optarg[i] > 57){
      fprintf (stderr,"Time must be in tens of milliseconds\n");
      return -1;
      }
    }
  crectime=atoi(optarg);
  if (crectime > 8640000 ) {
    fprintf (stderr,"Entered time must be less than 8640001\n");
    return -1;
  }
  return crectime;
}

int getclineno() {
  for (i = 0; optarg[i] != '\0'; i++) {
    if (optarg[i] == ' ' || optarg[i] == ',') {
      lineno = -2;
    } else {
      lineno = getcline(optarg[i]);
    } 
    if (lineno == -1) {
      /* The Entered Line is invalid */
      return -1;
    }
  if (lineno != -2) {
    validno = 1;
    acclineno = acclineno * 16 + lineno;
    if (acclineno > 255) {
      /* The Entered Line is out of Range */
      return -1;
    } 
  } else if (validno == 1) {
    /* lineno = -2 here, so we have a ' ' or ','*/
    qlines++;
    z = realloc (z, sizeof(int) * qlines);
    if (z == 0) {
      perror ("malloc failed");
      abort ();
    }
    z[qlines] = acclineno;
    validno = -1;
    acclineno = 0;
  }
  }
  if (validno != -1) {
    /* We finised off with a valid number but we have not copied it yet*/
    qlines++;
    z = realloc (z, sizeof(int) * qlines);
    if (z == 0) {
      perror ("malloc failed");
      abort ();
    }
    z[qlines] = acclineno;
  }
  validno = -1;
  acclineno = 0;
  return 0;
}

int getxlineno() {
  for (i = 0; optarg[i] != '\0'; i++) {
    if (optarg[i] == ' ' || optarg[i] == ',') {
      lineno = -2;
    } else {
      lineno = getcline(optarg[i]);
    } 
    if (lineno == -1) {
      /* The Entered Line is invalid */
      return -1;
    }
  if (lineno != -2) {
    validno = 1;
    acclineno = acclineno * 16 + lineno;
    if (acclineno > 255) {
      /* The Entered Line is out of Range */
      return -1;
    } 
  } else if (validno == 1) {
    /* lineno = -2 here, so we have a ' ' or ','*/
    qxlines++;
    zx = realloc (zx, sizeof(int) * qxlines);
    if (zx == 0) {
      perror ("malloc failed");
      abort ();
    }
    zx[qxlines] = acclineno;
    validno = -1;
    acclineno = 0;
  }
  }
  if (validno != -1) {
    /* We finised off with a valid number but we have not copied it yet*/
    qxlines++;
    zx = realloc (zx, sizeof(int) * qxlines);
    if (zx == 0) {
      perror ("malloc failed");
      abort ();
    }
    zx[qxlines] = acclineno;
  }
  validno = -1;
  acclineno = 0;
  return 0;
}

int validdigit() {
  for (i = 0; *(optarg + i) != '\0'; i++) {
    if (*(optarg + i) < 48 || *(optarg + i) > 57) {
      return 1;
    }
  count = count * 10 + *(optarg + i) - 48;
  if (count < 0) {
    /* We are beyond the integer limit */
    return 2;
  }
  }
  return 0;
}

int main (int argc, char *argv[])
{
  int recordcount = 0;
  int invalid = 0;
  int acount = 0;
  int zcount;
  int zflag = 1; /* zflag will be 1 if -z was not selected*/
  int orecordcount = 0;
  int bcount = 0;
  int ycount ;
  int yflag = 1; /* yflag will remain 1 if -y will not be selected*/
  while (1)
  {
  static struct option long_options[] =
    {
    /* These options don’t set a flag.
       We distinguish them by their indices. */
      {"version",     no_argument,       0, 'v'},
      {"help",  no_argument,       0, 'h'},
      {"inputfile",  required_argument, 0, 'i'},
      {"outputfile",  required_argument, 0, 'o'},
      {"line",    required_argument, 0, 'l'},
      {"exclude",    required_argument, 0, 'x'},
      {"start",    required_argument, 0, 's'},
      {"end",    required_argument, 0, 'e'},
      {"Start",    required_argument, 0, 'S'},
      {"End",    required_argument, 0, 'E'},
      {"icountstart",    required_argument, 0, 'a'},
      {"icountstop",    required_argument, 0, 'z'},
      {"ocountstart",    required_argument, 0, 'b'},
      {"ocountstop",    required_argument, 0, 'y'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;
    d = getopt_long (argc, argv, "vhi:o:l:x:s:e:S:E:a:z:b:y:",
                       long_options, &option_index);
    /* Detect the end of the options. */
    if (d == -1) {
      break;
    }
    switch (d)
    {
      case 'v':
        printf ("ffprune version 2.0\n");
        return 0;
        break;
      case 'l':
        if (qxlines != 0) {
          fprintf(stderr, "The -l and -x arguments cannot be used on the same run\n");
          return 1;
        }
        valid=getclineno();
        if (valid == -1) {
          fprintf (stderr,"Line Number out of Range (0 - FF)\n");
          return 1;
        }
        break;
      case 'x':
        if (qlines != 0) {
          fprintf(stderr, "The -l and -x arguments cannot be used on the same run\n");
          return 1;
        }
        valid=getxlineno();
        if (valid == -1) {
          fprintf (stderr,"Line Number out of Range (0 - FF)\n");
          return 1;
        }
        break;
      case 'h':
        printusage(0);
        return 0;
        break;
      case 'i':
        inputfile = optarg;
        f = fopen(inputfile, "r");
        if (f == 0) {
          fprintf (stderr, "Unable to open input file %s\n", inputfile);
          return 1;
        }
        break;
      case 'o':
        outputfile = optarg;
        g = fopen(outputfile, "w");
        if (g == 0) {
          fprintf (stderr, "Unable to open output file %s\n", outputfile);
          return 1;
        }
        break;
      case 'S':
        valid=checkargument(optarg);
        if ( valid == 1 ) {
          return 1;
        }
        Srectime=getrectime(optarg);
        if ( Srectime == -1 ) {
          return 1;
        }
        break;
      case 'E':
        valid=checkargument(optarg);
        if ( valid == 1 ) {
          return 1;
        }
        Erectime=getrectime(optarg);
        if ( Erectime == -1 ) {
          return 1;
        }
        break;
      case 's':
        srectime = checkdigits(optarg);
        if ( srectime == -1 ) {
          return 1;
        }
        break;
      case 'e':
        erectime = checkdigits(optarg);
        if ( erectime == -1 ) {
          return 1;
        }
        break;
      case 'a':
        count = 0;
        invalid = validdigit();
        if (invalid == 1) {
          fprintf(stderr, "Argument must be an integer\n");
          return 1; 
        }
        if (invalid == 2) {
          fprintf(stderr, "entered integer is out of limit\n");
          return 1; 
        }
        acount = count;
        break;
      case 'z':
        count = 0;
        invalid = validdigit();
        zflag = 0;
        if (invalid == 1) {
          fprintf(stderr, "Argument must be an integer\n");
          return 1;
        } 
        if (invalid == 2) {
          fprintf(stderr, "entered integer is out of limit\n");
          return 1;
        }
        zcount = count;
        break;
      case 'b':
        count = 0;
        invalid = validdigit();
        if (invalid == 1) {
          fprintf(stderr, "Argument must be an integer\n");
          return 1; 
        }
        if (invalid == 2) {
          fprintf(stderr, "entered integer is out of limit\n");
          return 1; 
        }
        bcount = count;
        break;
      case 'y':
        count = 0;
        invalid = validdigit();
        yflag = 0;
        if (invalid == 1) {
          fprintf(stderr, "Argument must be an integer\n");
          return 1;
        } 
        if (invalid == 2) {
          fprintf(stderr, "entered integer is out of limit\n");
          return 1;
        }
        ycount = count;
        break;
      default:
        fprintf(stderr, "Option is incorrect\n");
        return 1;
    }
  }
  if (f == 0) {
    fprintf (stderr, "No input file selected \n\n");
    printusage(1);
    return 1;
  }
  if (g == 0) {
    fprintf (stderr, "No output file selected \n\n");
    printusage(1);
    return 1;
  }
  if (srectime < Srectime) {
    srectime = Srectime; /* We arbiratily choose the biggest */
  }
  if (erectime > Erectime) {
    erectime = Erectime; /* We arbiratily choose the smallest */
  }
  while ( reclen != -1 ) {
    /* recordcount counts records of the inputfile, before any filtering 
       is executed. It will be used to filter against acount and zcount. */
    recordcount++;
    reclen=getreclen();
    if (emptyfile == 0 ) {
      /* if we are given an empty file emptyfile = 1
         yet reclen is still 0, so this seems the best way to deal with this
         exception, otherwise ffprune would not return error message and exit
         code */
      fprintf (stderr,"error: wrong file format.\n");
      error = 1;
    }
    if (reclen != -1 ) {
      y = realloc (y, sizeof(int) * (reclen - 11));
      if (y == 0) {
        perror ("malloc failed");
        abort ();
      }
      errorcode = geterrorcode();
      linenr = getlinenr();
      recordingday = getrecordingday();
      mmsb = getmmsb();
      msb = getmsb();
      lsb = getlsb();
      rectime = mmsb * 65536 + msb * 256 + lsb;
      hexdump(reclen);
      padding = getpadding();
      if (reclen == -1 || padding == 0) {
        reclen = -1;
        error = 1;
        if (emptyfile == 1) {
          fprintf (stderr,"error: wrong file format.\n");
          }
        else
          {
          /* emptyfile = 2, so there has been good records, but now we have problems */
          fprintf (stderr,"error: invalid record, file may corrupted.\n");
          }
        break;
      }
      if (zflag != 1 && recordcount > zcount) {
        /* zcount limit check*/
        return 0;
      }
      if (rectime > srectime && rectime < erectime && recordcount >= acount) {
        if (qlines == 0 && qxlines == 0) {
          /* No -l and -x arguments were chosen */
          orecordcount++;
          if (yflag != 1 && orecordcount > ycount) {
            return 0;
          }
          if (orecordcount >= bcount) {
            v = printdump(y);
          }
        } else if (qxlines == 0){
          /* -l argument was chosen */
          for (i = 1; i < qlines + 1; i++) {
            if (linenr == z[i]) {
              orecordcount++;
              if (yflag != 1 && orecordcount > ycount) {
                return 0;
              }
              if (orecordcount >= bcount) {
                v = printdump(y);
              }
            }
          }
        } else {
          /* -x argument was chosen */
          validno = -1;
          for (i = 1; i < qxlines + 1; i++) {
              if (linenr == zx[i]) {
                validno = 0;
            }
          }
          if (validno == -1) {
            orecordcount++;
            if (yflag != 1 && orecordcount > ycount) {
              return 0;
            }
            if (orecordcount >= bcount) {
              v = printdump(y);
            }
          }
        }
      }
      if (v != 0) {
        printusage(1);
        return 1;
      }
      /* At least one record was printed so emptyfile now has a value of 2 */
      emptyfile = 2;
    }
  }
  free(y);
  free(z);
  free (zx);
  fclose(f);
  if (outputfile != NULL) {
    fclose(g);
  }
  return error;
}
